FROM python:2.7.15-alpine3.6
MAINTAINER "<evalsocket@protonmail.com>"
RUN apk update && apk add gcc g++ linux-headers sqlite-dev
WORKDIR /app
COPY . /app

RUN pip install -r requirements.txt

EXPOSE 5000

ENTRYPOINT [ "python" ]
CMD ["fynd.py"]
