Start Application 

```
docker run -d --name fynd -p 80:5000 evalsocket/fynd

#Test API 
#Import Fynd_API in Postman
#Import Fynd_localhost env in postman
#Select Fynd_localhost env in postman
#Run Postman Runner for Fynd_API with Fynd_localhost env 

```

Docker Build 

```
git clone https://evalsocity@bitbucket.org/evalsocity/fynd.git
cd fynd
python init.py

docker login
docker build -t fynd .
docker tag fynd [username/image_name]
docker push [username/image_name]
docker run -d --name fynd -p 80:5000 [username/image_name]

```

Kunerneates Deployment 

```
# it's a optional step. By default it take my container image. if you build your docker image then you can change source from 'k8s/deplyment.yml' change from 'docker.io/evalsocket/fynd' to docker.io/[username/image_name] 
kubctl apply -f k8s/

#Test API 
#Import Fynd_API in Postman
#Import Fynd_Production env in postman
#Select Fynd_Production env in postman and change fynd_url & fynd_port
#Run Postman Runner for Fynd_API with Fynd_Production env 
# By Default Production env cluster can ready to scale with 2 nodes to 15 nodes. 

```

Heroku Deployment 

```
git clone https://evalsocity@bitbucket.org/evalsocity/fynd.git
cd fyndeval
heroku login
heroku create fyndeval
git push heroku master

```


Database

```
#I am using SQLite as datastore
#In a cluster database maintain individually by pods(nodes/server)

```

Authentication

```
#I am using basic auth with username & password
#For adding a layer of security i use passlib for encrypt password before storing users in database
#i use pbkdf2_sha256 
#We can also do JWT/firebase authentication 

```

Scaling Problem

```
Movies Data - 5M (50L)
Request per day - 15M per day
System For 5x of ideal case
Request Per day - 15M*5 = 75M(7.5Cr)per day
Benchmarking for Request per secound - 869 Request per secound
Expected Request = 174 per secound 


I deploy a kuberneates cluster it can scale from 2 node to 15 nodes
 1 node = 58 per sec (Benchmarking Number)
 1 node = 12 per sec (Expected)
 We don't have to pay for 15 nodes. We just have to pay for 2 nodes
 
With using mySQL db we can easly achive 3 writes and 4 reads. All id's should be index and name of movies also.

If we asume equal request then our per socound read and write will
Read - 27 per sec
Write - 36 per sec 

Expected read and writes
Reads - 5 around 
Write - 5 write 


Currently I didn't use mySQL. But Mysql benchmarking is more that that 


```