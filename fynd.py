#!flask/bin/python
from flask import Flask, jsonify, abort, request, make_response, url_for
from passlib.context import CryptContext
from sqlite3 import dbapi2 as sqlite
from functools import wraps
from flask_secure_headers.core import Secure_Headers


app = Flask(__name__, static_url_path="")
conn = sqlite.connect("movies.db", check_same_thread=False)
cur = conn.cursor()
sh = Secure_Headers()


pwd_context = CryptContext(
        schemes=["pbkdf2_sha256"],
        default="pbkdf2_sha256",
        pbkdf2_sha256__default_rounds=30000
)



def check_encrypted_password(password, hashed):
    return pwd_context.verify(password, hashed)


def encrypt_password(password):
    return pwd_context.encrypt(password)


def login_required(f):
    @wraps(f)
    def wrapped_view(**kwargs):
        auth = request.authorization
        cur.execute("select * from users where username='" +
                    auth.username + "'");
        result = cur.fetchone();
        # We can also do JWT/firebase authentication here
        if not (auth and result and check_encrypted_password(auth.password, result[1])):
            abort(401)

        return f(**kwargs)
    return wrapped_view


@app.errorhandler(404)
def page_not_found(e):
    return jsonify(error=404, text='Oppss 404'), 404


@app.route('/movies/api/v1.0/search/<string:q>', methods=['GET'])
@sh.wrapper()
def search_movies(q):
    if len(q) > 0 and len(q) < 10 and len(q):
        try:
           cur.execute(
               "select rowid,* from movies where name like '%" + q + "%'");
           result = cur.fetchall();
           if len(result) > 0:
               resp = [];
               for movie in result:
                   movie_data = {
                        'movie_id' : movie[0],
                        'name': movie[1],
                        'imdb_score': movie[2],
                        'genre': movie[3],
                        'director': movie[4],
                        'popularity': movie[5],
                    }
                   resp.append(movie_data)
           else:
               resp = [];
           return jsonify( { 'error': None, 'data':resp}), 200
        except:
            # log error
            abort(500)
    else:
        return jsonify( { 'error': "oppss voilating the rule", 'data':None}), 200

@app.route('/movies/api/v1.0/list', methods = ['GET'])
@sh.wrapper()
def get_movies():
    try:
       cur.execute("select rowid,* from movies");
       result = cur.fetchall();
       if len(result) > 0 :
           resp = [];
           for movie in result:
               movie_data = {
                    'movie_id' : movie[0],
                    'name': movie[1],
                    'imdb_score': movie[2],
                    'genre': movie[3],
                    'director': movie[4],
                    'popularity': movie[5],
                }
               resp.append(movie_data)
       else:
           resp = []
       return jsonify( { 'error': None, 'data':resp}), 200
    except ZeroDivisionError:
        # log error
        abort(500)


@app.route('/movies/api/v1.0/movie/<string:movie_id>', methods = ['GET'])
@sh.wrapper()
def get_movie(movie_id):
    if(movie_id):
        try:
           cur.execute("select rowid,* from movies where rowid='"+movie_id+"'");
           result = cur.fetchone();
           if result:
                movie_data = {
                    'movie_id' : result[0],
                    'name': result[1],
                    'imdb_score': result[2],
                    'genre': result[3],
                    'director': result[4],
                    'popularity': result[5]
                }
                return jsonify( { 'error': None,'data':movie_data}), 200                
           else:
                return jsonify( { 'error': 'No Result Found','data':None}), 200
        except:
            # log error
            abort(500)
    else:
        return jsonify( { 'error': 'Movie id is missing','data':None}), 200


@app.route('/movies/api/v1.0/movie', methods = ['POST'])
@sh.wrapper()
@login_required
def create_movie():
    if not request.json or not 'name' in request.json or not 'imdb_score' in request.json or not 'genre' in request.json or not 'director' in request.json or not 'popularity' in request.json:
        abort(400)
    try:
        cur.execute("insert into movies values ('"+request.json['name']+"','"+str(request.json.get('imdb_score'))+"','"+request.json['genre']+"','"+request.json['director']+"','"+str(request.json['popularity'])+"')");
        conn.commit()
        return jsonify( { 'error': None ,'data' : request.json } ), 200
    except ZeroDivisionError:
        print(ZeroDivisionError)
        # log error
        abort(500)


@app.route('/movies/api/v1.0/movie/<string:movie_id>', methods = ['PUT'])
@sh.wrapper()
@login_required
def update_movie(movie_id):

    if not movie_id or not request.json or not 'name' in request.json or not 'imdb_score' in request.json or not 'genre' in request.json or not 'director' in request.json or not 'popularity' in request.json:
        abort(400)
    try:
       cur.execute("update movies SET name='"+request.json['name']+"',imdb_score='"+str(request.json.get('imdb_score'))+"',genre='"+request.json['genre']+"',director='"+request.json['director']+"',popularity='"+str(request.json['popularity'])+"' where rowid="+movie_id+"");
       return jsonify( { 'error': None, 'data': request.json } ), 200
    except:
        # log error
        abort(500)



@app.route('/movies/api/v1.0/movie/<string:movie_id>', methods = ['DELETE'])
@sh.wrapper()
@login_required
def delete_movie(movie_id):
    if(movie_id):
        try:
           cur.execute("delete from movies where rowid= '"+movie_id+"'");
           return jsonify( { 'error': None }), 200
        except ZeroDivisionError:
            print(ZeroDivisionError)
            # log error
            abort(500)
    else:
        return jsonify( { 'error': 'oppss something happen wront', 'data': None } ), 200


if __name__ == '__main__':
    app.run(host='0.0.0.0')
