from passlib.context import CryptContext
from sqlite3 import dbapi2 as sqlite
import json

conn = sqlite.connect("movies.db")
cur = conn.cursor()

pwd_context = CryptContext(
        schemes=["pbkdf2_sha256"],
        default="pbkdf2_sha256",
        pbkdf2_sha256__default_rounds=30000
)

def encrypt_password(password):
    return pwd_context.encrypt(password)

# Create User table with column username,password,role and status......
# Password is hash encrypted
cur.execute("create table users (username varchar, password varchar, role integer, status integer)")
# Create Movies table with column name, imdb_score,genre,director,9999popularity
# every row has an id row_id
cur.execute("create table movies (name varchar, imdb_score integer, genre varchar,director varchar,popularity integer)")

#Read JSON data into the datastore variable
with open('imdb.json', 'r') as f:
    datastore = json.load(f)

#insert data in user table and password
password = encrypt_password("evalsocket")
cur.execute("insert into users values ('evalsocket','"+password+"',1,1)");
conn.commit()
i = 0;
sep = ","

for movie in datastore:
    cur.execute("insert into movies values ('"+movie['name']+"',"+str(movie['imdb_score'])+",'"+sep.join(movie['genre'])+"','"+movie['director']+"',"+str(movie['99popularity'])+")");
    conn.commit()
    i=i+1;
print("Total Movies : ",i)
